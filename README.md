# Movie Raiders

# Meet the Team

Laura McDonald
Timothee Mbutcho
Farell Febriano

## Design

Our design principles are focused on delivering a seamless experience for the users. Explore our designs below:

- **UI/UX Design**: ![View Design](assets/exalidraw-movie-raiders-light.png)
- **API Preliminary Design**: [Google Doc Design](https://docs.google.com/document/d/1dixCSWywJBLrmTVfetDRPna-jHAqebuNpoyTkX_wKWI/edit#heading=h.yhxc715rlusf)

Movie Raiders - bringing you all the movies that stole your heart.
Movie Raiders - bringing you all the movies that stole your heart.
## Intended Market

Movie Raiders is designed for general consumers of various streaming services. Whether you're an avid binge-watcher or a casual viewer, our platform offers an interactive way to make a watchlist, comment on movies and pick your favorite genres. Dive into a community that values genuine opinions and discover films that resonate with your tastes.

## Functionality
1. users can select there favorite genres upon signing up for an account.

2. movies from the users favorite genres are displayed on the homepage, for a unique and customized experience.

3. Users can click on the plus icon on any movie in the homepage, in order to add that movie to their watchlist.

4. users can click on watchlist, in the navbar, and be redirected to theeir wathclist.

5. users can click on the delete button under a watchlist movie in order to remove it from their watchlist.

6. users can click on a movie in the homepage and be redirected to that movie's detail.

7. In a movie detail's page, they will have acess to trailers, a synopsis, pictures, as well as information on the cast.

8. Users are also able to leave comments under a movie.

9. A user can click on Account, located in the nav bar, which redirects them to their My Account Page.

10. In the My Account Page, a user can update their email and username, as well as add or remove their favorite genres.

11. A user can also see the history of comments they have made.



1. movies from the users favorite genres are displayed on the homepage, for a unique and customized experience.

2. Users can click on the plus icon twice, on any movie in the homepage, in order to add that movie to their watchlist.

3. users can click on watchlist, in the navbar, and be redirected to their wathclist.

4. users can click on the delete button under a watchlist movie in order to remove it from their watchlist.

5. users can click on a movie in the homepage and be redirected to that movie's detail.

6. In a movie detail's page, they will have acess to trailers, a synopsis, pictures, as well as information on the cast.

7. Users are also able to leave comments under a movie.

8. A user can click on Account, located in the nav bar, which redirects them to their My Account Page.

9. In the My Account Page, a user can update their email and username, as well as add or remove their favorite genres.

10. A user can also see the history of comments they have made, and a link to the film they commented on.



## Project Initialization

To set up and run the Movie Raiders project locally, follow these steps:

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/two-spirit-penguins/module3-project-gamma.git
   ```

2. Navigate into the repository:

   ```bash
   cd module3-project-gamma
   ```

3. Modify the `Dockerdev.env` file to include a valid token for TMBD.

4. Run the application using Docker:

   ```bash
   docker compose up -d
   ```

5. Access the application in your browser at: [http://localhost:3000](http://localhost:3000)

## Future Plans

1. We would like to add a way for the user to rate movies.
2. We would like to add a original logo.
3. We would like to send an email to a user whose comment was replied to.

1. We would like to add a way for the user to rate movies.
2. We would like to add a original logo.
3. We would like to send an email to a user whose comment was replied to.


## Known Issues

1. The Trailer button on the homepage does not function.

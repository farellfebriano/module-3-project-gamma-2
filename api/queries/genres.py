from queries.pool import pool
from models.genres import (
    ApiGenresList,
    ApiGenresOut,
    FavoriteGenresIn,
    FavoriteGenresOut,
    LisGenresOut,
)


class GenresRepository:
    def list_api_genre_obj(self, result):
        lis = []
        for content in result:
            obj = ApiGenresOut(
                name=content[1], api_genre_id=content[0], id=content[2]
            )
            lis.append(obj)
        return lis

    def get_all_api_genre_id(self):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT api_genre_id, name, id
                    FROM api_genres;
                    """
                )
                lis = self.list_api_genre_obj(result)
                return ApiGenresList(api_genres=lis)

    def create_list_favorite_genres(
        self, favorite_genres_in: FavoriteGenresIn, account_id: int
    ):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO favorite_genres
                    (api_genre_id, account_id)
                    VALUES
                        (%s, %s)
                    RETURNING id;
                    """,
                    (favorite_genres_in.api_genre_id, account_id),
                )
                id = result.fetchone()[0]
                favorite_genres_out = FavoriteGenresOut(
                    api_genre_id=favorite_genres_in.api_genre_id,
                    account_id=account_id,
                    id=id,
                )
                return favorite_genres_out

    def id_list_of_fav_genres(self, genres_ids, account_id):
        lis = []

        for i in genres_ids:
            dic = {
                "id": None,
                "api_genre_id": None,
                "account_id": None,
                "name": None,
            }
            dic["id"] = i[0][1]
            dic["api_genre_id"] = i[0][0]
            dic["account_id"] = account_id
            dic["name"] = i[0][2]
            lis.append(dic)

        return lis

    def get_list_favorite_genres_by_id(self, account_id: int):
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT (f.api_genre_id,f.id,a.name)
                    FROM favorite_genres f
                    JOIN api_genres a on f.api_genre_id=a.api_genre_id
                    WHERE account_id=%s
                    """,
                    [account_id],
                )
                lis_fav_genres = self.id_list_of_fav_genres(result, account_id)
                return LisGenresOut(favorite_genres=lis_fav_genres)

    def delete_favorite_genres(self, favorite_genres_id, account_id):
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM favorite_genres
                    WHERE id=%s AND account_id=%s
                    """,
                    [favorite_genres_id, account_id],
                )
                return True

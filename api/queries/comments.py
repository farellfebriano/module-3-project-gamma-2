from queries.pool import pool
from typing import List
from models.comments import (
    CommentIn,
    CommentOut,
    CommentUpdate,
    Message,
    CommentOutWithUsername,
)


class CommentRepository:
    def create_comment(
        self, comment: CommentIn, account_id: int
    ) -> CommentOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO comments (comment, movie_api_id, account_id)
                    VALUES
                        (%s, %s, %s)
                    RETURNING id;
                    """,
                    (
                        comment.comment,
                        comment.movie_api_id,
                        account_id,
                    ),
                )
                id = result.fetchone()[0]
                comment_out = CommentOut(
                    id=id,
                    comment=comment.comment,
                    movie_api_id=comment.movie_api_id,
                    account_id=account_id,
                )
            return comment_out

    def get_comments_by_account_id(self, account_id: int) -> List[CommentOut]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    SELECT id, comment, movie_api_id, account_id
                    FROM comments
                    WHERE account_id = %s;
                    """,
                    (account_id,),
                )
                comments = []
                for data in result.fetchall():
                    comment_out = CommentOut(
                        id=data[0],
                        comment=data[1],
                        movie_api_id=data[2],
                        account_id=data[3],
                    )
                    comments.append(comment_out)
                return comments

    # NOTE --> i need to change the api id into string because everting need to be in the string
    # later on we need to change the database it self

    def get_comments_by_movie_api_id(
        self, movie_api_id: int
    ) -> List[CommentOutWithUsername]:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                        SELECT c.id, c.comment, c.movie_api_id, a.username
                        FROM comments c
                        JOIN accounts a ON c.account_id = a.id
                        WHERE movie_api_id = %s;
                        """,
                    (movie_api_id,),
                )
                comments = []

                for data in result.fetchall():
                    comment_out = CommentOutWithUsername(
                        id=str(data[0]),
                        comment=data[1],
                        movie_api_id=data[2],
                        username=data[3],
                    )
                    comments.append(comment_out)
                return comments

    def update_comment(
        self,
        comment_id: int,
        updated_comment_data: CommentUpdate,
        account_id: int,
    ) -> CommentOut:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    UPDATE comments
                    SET comment = %s
                    WHERE id = %s AND account_id =%s
                    RETURNING id, comment, movie_api_id, account_id;
                    """,
                    (
                        updated_comment_data.updated_comment,
                        comment_id,
                        account_id,
                    ),
                )
                data = result.fetchone()
                comment_out = CommentOut(
                    id=data[0],
                    comment=data[1],
                    movie_api_id=data[2],
                    account_id=data[3],
                )
                return comment_out

    def delete_comment(self, comment_id: int, account_id: int) -> Message:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    DELETE FROM comments
                    WHERE id = %s AND account_id = %s;
                    """,
                    (comment_id, account_id),
                )
        return Message(message="Comment successfully deleted")

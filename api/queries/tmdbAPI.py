import requests
import random


class TMDBRepository:
    def randomnizer(self):
        page = random.randint(1, 6)
        return page

    def list_movie_sorted_by_apiId(self, api_id):
        url = f"https://api.themoviedb.org/3/movie/{api_id}?language=en-US"
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5N2UwNGNlNjQzMzNkYWEyOTMwZWQ1Y2RjMTU5MmI3NCIsInN1YiI6IjY0ZDQxOWJjYmYzMWYyMDFjYjY4NGI1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HVT8v4RpWyhLj85b6rd6VLcBDxgjoWIVm1QIi1LuVOE",
        }
        movie = {}
        movie = {}
        response = requests.get(url, headers=headers)

        response_json = response.json()
        title = response_json["original_title"]
        poster_path = response_json["poster_path"]
        overview = response_json["overview"]
        id = response_json["id"]
        genres = response_json["genres"]

        backdrop_path = response_json["backdrop_path"]
        poster = f"https://image.tmdb.org/t/p/original/{poster_path}"
        release_date = response_json["release_date"]
        backdrop = f"https://image.tmdb.org/t/p/original/{backdrop_path}"

        movie["title"] = title
        movie["poster"] = poster
        movie["overview"] = overview
        movie["id"] = id
        movie["genres"] = genres
        movie["release_date"] = release_date
        movie["backdrop"] = backdrop
        return movie

    def list_movie_sorted_by_genresId(self, genre_id):
        url = f"https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=en-US&sort_by=popularity.desc&with_genres={genre_id}"
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5N2UwNGNlNjQzMzNkYWEyOTMwZWQ1Y2RjMTU5MmI3NCIsInN1YiI6IjY0ZDQxOWJjYmYzMWYyMDFjYjY4NGI1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HVT8v4RpWyhLj85b6rd6VLcBDxgjoWIVm1QIi1LuVOE",
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()
        return response_json

    def list_now_playing_movie(self):
        page = self.randomnizer()
        url = f"https://api.themoviedb.org/3/movie/now_playing?language=en-US&page={page}"
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5N2UwNGNlNjQzMzNkYWEyOTMwZWQ1Y2RjMTU5MmI3NCIsInN1YiI6IjY0ZDQxOWJjYmYzMWYyMDFjYjY4NGI1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HVT8v4RpWyhLj85b6rd6VLcBDxgjoWIVm1QIi1LuVOE",
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()
        return response_json["results"]

    def list_top_rated_movie(self):
        page = self.randomnizer()
        url = f"https://api.themoviedb.org/3/movie/top_rated?language=en-US&page={page}"
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5N2UwNGNlNjQzMzNkYWEyOTMwZWQ1Y2RjMTU5MmI3NCIsInN1YiI6IjY0ZDQxOWJjYmYzMWYyMDFjYjY4NGI1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HVT8v4RpWyhLj85b6rd6VLcBDxgjoWIVm1QIi1LuVOE",
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()
        return response_json["results"]

    def list_popular_movie(self):
        page = self.randomnizer()
        url = f"https://api.themoviedb.org/3/movie/popular?language=en-US&page={page}"
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5N2UwNGNlNjQzMzNkYWEyOTMwZWQ1Y2RjMTU5MmI3NCIsInN1YiI6IjY0ZDQxOWJjYmYzMWYyMDFjYjY4NGI1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HVT8v4RpWyhLj85b6rd6VLcBDxgjoWIVm1QIi1LuVOE",
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()
        return response_json["results"]

    def list_upcoming_movie(self):
        page = self.randomnizer()
        url = f"https://api.themoviedb.org/3/movie/upcoming?language=en-US&page={page}"
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5N2UwNGNlNjQzMzNkYWEyOTMwZWQ1Y2RjMTU5MmI3NCIsInN1YiI6IjY0ZDQxOWJjYmYzMWYyMDFjYjY4NGI1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HVT8v4RpWyhLj85b6rd6VLcBDxgjoWIVm1QIi1LuVOE",
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()
        return response_json["results"]

    def cast_by_movie_id(self, api_id):
        url = f"https://api.themoviedb.org/3/movie/{api_id}/credits?language=en-US"
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5N2UwNGNlNjQzMzNkYWEyOTMwZWQ1Y2RjMTU5MmI3NCIsInN1YiI6IjY0ZDQxOWJjYmYzMWYyMDFjYjY4NGI1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HVT8v4RpWyhLj85b6rd6VLcBDxgjoWIVm1QIi1LuVOE",
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()
        return response_json["cast"]

    def get_video_by_movie_id(self, api_id):
        url = f"https://api.themoviedb.org/3/movie/{api_id}/videos?language=en-US"
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5N2UwNGNlNjQzMzNkYWEyOTMwZWQ1Y2RjMTU5MmI3NCIsInN1YiI6IjY0ZDQxOWJjYmYzMWYyMDFjYjY4NGI1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HVT8v4RpWyhLj85b6rd6VLcBDxgjoWIVm1QIi1LuVOE",
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()
        return response_json["results"]

    def get_backdrops_by_movie_id(self, api_id):
        url = f"https://api.themoviedb.org/3/movie/{api_id}/images"
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5N2UwNGNlNjQzMzNkYWEyOTMwZWQ1Y2RjMTU5MmI3NCIsInN1YiI6IjY0ZDQxOWJjYmYzMWYyMDFjYjY4NGI1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HVT8v4RpWyhLj85b6rd6VLcBDxgjoWIVm1QIi1LuVOE",
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()
        return response_json["backdrops"]

    def get_movie_by_keywords(self, keywords):
        url = f"https://api.themoviedb.org/3/search/movie?query={keywords}&include_adult=false&language=en-US&page=1"
        headers = {
            "accept": "application/json",
            "Authorization": "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5N2UwNGNlNjQzMzNkYWEyOTMwZWQ1Y2RjMTU5MmI3NCIsInN1YiI6IjY0ZDQxOWJjYmYzMWYyMDFjYjY4NGI1YyIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.HVT8v4RpWyhLj85b6rd6VLcBDxgjoWIVm1QIi1LuVOE",
        }
        response = requests.get(url, headers=headers)
        response_json = response.json()

        return response_json

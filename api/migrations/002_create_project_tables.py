steps = [
    [
        """
        CREATE TABLE api_genres (
            id SERIAL PRIMARY KEY NOT NULL,
            name VARCHAR(20) NOT NULL,
            api_genre_id INT NOT NULL UNIQUE
        );
        """,

        """
        DROP TABLE api_genres;
        """
    ],

    [
        """
        CREATE TABLE favorite_genres (
            id SERIAL PRIMARY KEY NOT NULL,
            api_genre_id INT NOT NULL REFERENCES api_genres(api_genre_id),
            account_id INT NOT NULL REFERENCES accounts(id)
        );
        """,

        """
        DROP TABLE favorite_genres;
        """
    ],

    [
        """
        CREATE TABLE comments (
            id SERIAL PRIMARY KEY NOT NULL,
            comment VARCHAR(2000) NOT NULL,
            movie_api_id INT NOT NULL,
            account_id INT NOT NULL REFERENCES accounts(id)
        );
        """,

        """
        DROP TABLE comments;
        """
    ],

    [
        """
        CREATE TABLE watch_lists (
            id SERIAL PRIMARY KEY NOT NULL,
            movie_api_id INT NOT NULL,
            account_id INT NOT NULL REFERENCES accounts(id)
        );
        """,

        """
        DROP TABLE watch_lists;
        """
    ],



]

# python -m pytest is to run the test(do it in container terminal)
# do it from the api level directory, don't go in the test directory.

from fastapi.testclient import TestClient
from main import app
from queries.watch_list import WatchListRepository
from authenticator import authenticator
from models.watch_list import WatchListIn, WatchListOut

client = TestClient(app)


def fake_get_current_account_data():
    return {"id": 7777, "username": "timotheee"}


class FakeWatchlistRepository:
    def create_watch_list(self, watch_list: WatchListIn, account_id: int):
        watch_list_item = watch_list.dict()
        watch_list_item["account_id"] = account_id
        watch_list_item["id"] = 18
        watch_list_item["movie_api_id"] = watch_list.movie_api_id
        return WatchListOut(**watch_list_item)


def test_create_watch_list():
    app.dependency_overrides[WatchListRepository] = FakeWatchlistRepository
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_get_current_account_data
    body = {"movie_api_id": 100}
    res = client.post("/api/watchlists", json=body)
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "movie_api_id": 100,
        "id": 18,
        "account_id": 7777,
    }

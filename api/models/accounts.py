from pydantic import BaseModel, EmailStr, validator
from jwtdown_fastapi.authentication import Token


class AccountForm(BaseModel):
    username: str
    password: str
    confirm_password: str
    email: str


class AccountIn(BaseModel):
    username: str
    password: str
    confirm_password: str
    email: str

    @validator("confirm_password", pre=True, always=True)
    def validate_passwords_match(cls, confirm_password, values):
        if "password" in values and confirm_password != values["password"]:
            raise ValueError("Passwords do not match")
        return confirm_password


class AccountOut(BaseModel):
    id: str
    username: str
    email: str


class AccountOutWithHashedPassword(AccountOut):
    id: str
    username: str
    hashed_password: str
    email: str


class AccountUpdateUsername(BaseModel):
    username: str


class AccountUpdatePassword(BaseModel):
    password: str
    confirm_password: str

    @validator("confirm_password", pre=True, always=True)
    def validate_passwords_match(cls, confirm_password, values):
        if "password" in values and confirm_password != values["password"]:
            raise ValueError("Passwords do not match")
        return confirm_password


class AccountUpdateEmail(BaseModel):
    email: EmailStr


class AccountToken(Token):
    account: AccountOut


class Message(BaseModel):
    message: str

from fastapi import APIRouter, Depends, Request, Response
from authenticator import authenticator
from models.accounts import (
    AccountToken,
    AccountIn,
    AccountOut,
    AccountForm,
    AccountUpdateUsername,
    AccountUpdatePassword,
    AccountUpdateEmail,
    Message,
    AccountToken,
)
from queries.accounts import AccountQueries
from authenticator import authenticator
from typing import Union
from pydantic import BaseModel
from psycopg.errors import UniqueViolation


router = APIRouter()


class HttpError(BaseModel):
    detail: str


@router.post("/api/accounts", response_model=Union[AccountToken, Message])
async def create_account(
    info: AccountIn,
    request: Request,
    response: Response,
    queries: AccountQueries = Depends(),
):
    hashed_password = authenticator.hash_password(info.password)
    try:
        account = queries.create_account(info, hashed_password)
    except UniqueViolation:
        return {"message": "Username already exists"}
    form = AccountForm(
        username=info.username,
        password=info.password,
        confirm_password=info.confirm_password,
        email=info.email,
    )
    token = await authenticator.login(response, request, form, queries)
    return AccountToken(account=account, **token.dict())


@router.patch(
    "/api/accounts/password",
    response_model=Union[AccountUpdatePassword, AccountOut],
)
def update_account_password(
    info: AccountUpdatePassword,
    repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    hashed_password = authenticator.hash_password(info.password)
    return repo.update_password(account_id, hashed_password)


@router.patch(
    "/api/accounts/email",
    response_model=Union[AccountUpdateEmail, AccountOut],
)
async def update_account_email(
    info: AccountUpdateEmail,
    repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> AccountOut:
    account_id = account_data["id"]
    return repo.update_email(account_id, info.email)


@router.patch(
    "/api/accounts/username",
    response_model=Union[AccountUpdateUsername, AccountOut],
)
async def update_account_username(
    info: AccountUpdateUsername,
    repo: AccountQueries = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> AccountOut:
    account_id = account_data["id"]
    return repo.update_username(account_id, info.username)


@router.get("/token", response_model=AccountToken | None)
async def get_token(
    request: Request,
    account: AccountOut = Depends(authenticator.try_get_current_account_data),
) -> AccountToken | None:
    if account and authenticator.cookie_name in request.cookies:
        return {
            "access_token": request.cookies[authenticator.cookie_name],
            "type": "Bearer",
            "account": account,
        }

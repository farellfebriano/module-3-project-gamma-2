from fastapi import APIRouter, Depends
from authenticator import authenticator
from pydantic import BaseModel
from typing import Union
from models.genres import (
    ApiGenresList,
    FavoriteGenresIn,
    FavoriteGenresOut,
    LisGenresOut,
    Message,
)


class something(BaseModel):
    bla: str


from queries.genres import GenresRepository

router = APIRouter()


@router.get("/api/genres", response_model=ApiGenresList)
def get_all_api_genre_id(repo: GenresRepository = Depends()):
    return repo.get_all_api_genre_id()


@router.post("/api/favorite_genres", response_model=FavoriteGenresOut)
def create_list_favorite_genres(
    favorite_genres_in: FavoriteGenresIn,
    repo: GenresRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    account_id = account_data["id"]
    return repo.create_list_favorite_genres(favorite_genres_in, account_id)


@router.get("/api/favorite_genres/mine", response_model=LisGenresOut)
def get_favorite_genres_byId(
    repo: GenresRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> FavoriteGenresOut:
    account_id = account_data["id"]
    return repo.get_list_favorite_genres_by_id(account_id)


@router.delete(
    "/api/favorite_genres/{favorite_genres_id}",
    response_model=Union[bool, Message],
)
def delete_favorite_genres(
    favorite_genres_id: int,
    repo: GenresRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> Union[Message, bool]:
    account_id = account_data["id"]
    return repo.delete_favorite_genres(favorite_genres_id, account_id)

import {
  useGetWatchListQuery,
  useDeleteWatchlistMutation,
} from "../store/ApiMovies";

import { useNavigate } from "react-router-dom";
import "./Watchlist.css";

import { useEffect, useState } from "react";
import Nav from "../Nav/Nav";
import Fetching from "./fetching";
import Footer_Footer from "../HomePage/Footer_Footer";

function MyWatchList() {
  const { data, error, isLoading } = useGetWatchListQuery();

  const [deleteWatchlistItem, deleteWalistItemResult] =
    useDeleteWatchlistMutation();

  const deleteInputHandler = async (event) => {
    const value = event.target.value;
    try {
      await deleteWatchlistItem(value);
    } catch (error) {
      console.error(error);
    }
  };
  if (isLoading) return <>Loading...</>;
  if (error)
    return <>There was a small problem, it will be solved shortly.......</>;
  return (
    <>
      <Nav />
      <h3 className=" mywatchlist__title">My Watchlist</h3>
      <div className="watch-movies-container ">
        {data.watch_lists.map((movie) => (
          <div key={movie.id}>
            <Fetching id={movie.movie_api_id} />
            <button
              value={movie.id}
              onClick={deleteInputHandler}
              className="erase-button"
            >
              Delete
            </button>
          </div>
        ))}
      </div>
    </>
  );
}

export default MyWatchList;

import { useEffect, useState } from "react";
import { useGetMovieById_alteredQuery } from "../store/ApiMovies";
import "./Watchlist.css";
function Fetching({ id }) {
  const { data: content } = useGetMovieById_alteredQuery(id);
  return (
    <div className="watch-movies">
      <img
        key={content?.id}
        className="rowrow__poster__picture"
        alt=""
        src={content?.poster}
      ></img>
    </div>
  );
}
export default Fetching;

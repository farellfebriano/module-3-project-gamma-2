import { useState, useEffect } from "react";
import { useSignupMutation } from "../store/ApiMovies";
import { useNavigate } from "react-router-dom";
import "./Signup.css";
import Nav from "../Nav/Nav";

function SignUp() {
  const [SignUp, result] = useSignupMutation();
  const [username, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [confirm_password, setConfirmPasword] = useState("");
  const [email, setEmail] = useState("");
  const [signupError, setSignupError] = useState("");

  const navigate = useNavigate();

  const userNameHandler = (event) => {
    setUserName(event.target.value);
  };

  const userPasswordHandler = (event) => {
    setPassword(event.target.value);
  };

  const userConfirmPaswordHandler = (event) => {
    setConfirmPasword(event.target.value);
  };

  const userEmailHandler = (event) => {
    setEmail(event.target.value);
  };

  function submitHandler(event) {
    event.preventDefault();
    SignUp({ username, password, confirm_password, email });
  }

  useEffect(() => {
    if (result.isSuccess) {
      setUserName("");
      setPassword("");
      setConfirmPasword("");
      setEmail("");
      navigate("/");
    } else if (result.isError) {
      setSignupError("Invalid attempt. Please try again.");
    }
  }, [result]);

  return (
    <>
      <Nav />
      <div className="signup-container">
        <div className="signup-row">
          <div className="offset-3 col-6 signup-row__block">
            <div className="shadow p-4 signup-row__upper__block  mt-7">
              <h1 className="signup__title" id="signup__title">
                SignUp
              </h1>
              {signupError && <p className="text-danger">{signupError}</p>}
              <form id="create-location-form" onSubmit={submitHandler}>
                <div className="form-floating mb-3">
                  <input
                    placeholder="Name"
                    required
                    type="text"
                    name="username"
                    id="username"
                    className="form-control"
                    onChange={userNameHandler}
                    value={username}
                  />
                  <label htmlFor="name">Username</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    placeholder="Email"
                    required
                    type="text"
                    name="email"
                    id="email"
                    className="form-control"
                    onChange={userEmailHandler}
                    value={email}
                  />
                  <label htmlFor="email">Email</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    placeholder="Password"
                    required
                    type="password"
                    name="password"
                    id="password"
                    className="form-control"
                    onChange={userPasswordHandler}
                    value={password}
                  />
                  <label htmlFor="password">Password</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    placeholder="Confirm Password"
                    required
                    type="password"
                    name="confirm_password"
                    id="confirm_password"
                    className="form-control"
                    onChange={userConfirmPaswordHandler}
                    value={confirm_password}
                  />
                  <label htmlFor="confirm_password">Confirm Password</label>
                </div>
                <div className="mb-3"></div>
                <button className="movie-login-btn-dark-blue btn">
                  SignUp
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SignUp;

import { useGetVideoByApiIdQuery } from "../store/ApiMovies";
import "./Trailer.css";
function Trailer(props) {
  const { movie_api_id } = props;
  const { data: content } = useGetVideoByApiIdQuery(movie_api_id);

  return (
    <>
      <h1 className="mv-title__component">Trailer</h1>
      <div className="mv-trailer__container">
        <iframe
          className="mv-traile"
          width="1060"
          height="615"
          src={`https://www.youtube.com/embed/${content}?si=EW3jNg9MUImgtifR`}
          title="YouTube video player"
          frameBorder="0"
          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
          allowFullScreen
        ></iframe>
      </div>
    </>
  );
}
export default Trailer;

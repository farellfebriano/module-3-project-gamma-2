import React from "react";
import "./CommentSection.css";
import {
  useGetCommentsByMovieApiIdQuery,
  useGetTokenQuery,
  useCreateCommentMutation,
  useUpdateCommentMutation,
  useDeleteCommentMutation,
} from "../store/ApiMovies";
import { useEffect, useState } from "react";
function initial(string) {
  const initial = string[0];
  return initial.toUpperCase();
}

function CommentSection(props) {
  const { movie_api_id } = props;
  // creating an input handler that
  // will change comeshow into input
  const [comment_id, setComment_Id] = useState("");
  const inputHandler = (event) => {
    const value = event.target.value;
    setComment_Id(value);
  };

  // creating the update
  const [updated_comment, setUpdatedComment] = useState("");
  const updateCommentHandler = (event) => {
    const value = event.target.value;
    setUpdatedComment(value);
  };

  const [comment, setComment] = useState("");
  const commentHandler = (event) => {
    const value = event.target.value;
    setComment(value);
  };

  const { data: comments, isLoading } =
    useGetCommentsByMovieApiIdQuery(movie_api_id);
  const { data: token } = useGetTokenQuery();
  const [postComment, postCommentResult] = useCreateCommentMutation();
  const [postUpdatedComment, postUpdatedCommentResult] =
    useUpdateCommentMutation();
  const [deleteComment, deleteCommentResult] = useDeleteCommentMutation();

  // creating update comment handling

  //creating the submit handler for the post
  function postHandler(event) {
    event.preventDefault();

    postComment({ comment, movie_api_id });
    setComment("");

    // checking the post is succes
    if (postCommentResult.isSuccess) {
    } else if (postCommentResult.isError) {
    }
  }

  function UpdateHandler(event) {
    event.preventDefault();
    postUpdatedComment({ comment_id, updated_comment });
    setComment_Id(0);
  }
  // checking the update
  if (postUpdatedCommentResult.isSuccess) {
  } else if (postUpdatedCommentResult.isError) {
  } else if (postUpdatedCommentResult.isUninitialized) {
  }
  // DELETE section
  const deleteInputHandler = (event) => {
    const value = event.target.value;
    event.preventDefault();
    // setComment_IdDelete(value);
    deleteComment({ value });
  };
  if (deleteCommentResult.isSuccess) {
  } else if (deleteCommentResult.isError) {
  } else if (deleteCommentResult.isUninitialized) {
  }

  return (
    <>
      <h1 className="mv-title__component"> Comment</h1>
      <div className="container center__display">
        <div className="top">
          <div className="video__container">
            <div className="form-floating">
              <form onSubmit={postHandler}>
                <textarea
                  onChange={commentHandler}
                  value={comment}
                  className="form-control"
                  placeholder={
                    token ? "Leave a comment here" : "Login for comment"
                  }
                  id="floatingTextarea"
                  // if the user then it disabeled
                  disabled={!token}
                ></textarea>
                <button id="form-button" className="form-button" type="submit">
                  Submit
                </button>
              </form>
            </div>
          </div>
        </div>
        {isLoading ||
        postCommentResult.isLoading ||
        postUpdatedCommentResult.isLoading ? (
          <></>
        ) : (
          <div className={comments.length > 5 && "scroll-bg"}>
            <div className={comments.length > 5 && "scroll-div"}>
              <div className="scroll-object">
                {comments.map((d) => {
                  return (
                    <div
                      key={d.id}
                      className="comments__container center__display"
                    >
                      <div className="comment__card">
                        <div className="pic center__display">
                          {initial(d.username)}
                          {/* <div className="image-container">
                    <img src="https://encrypted-tbn0.gstatic.com/licensed-image?q=tbn:ANd9GcRq7GL-4F1WG8y0VvoAL4thNABtg19U1kFSe5a6mVueRqK344JmwL_o2Xs2RwMWN6-AOrueJQa28di4XoE"></img>
                  </div> */}
                        </div>
                        <form>
                          <div className="comment__info">
                            <small id="username_comment_section">
                              {d.username}
                            </small>
                            {d.id === comment_id ? (
                              <>
                                <textarea
                                  onChange={updateCommentHandler}
                                  value={updated_comment}
                                  className="form-control"
                                  placeholder={d.comment}
                                  id="floatingTextarea_2"
                                ></textarea>
                                <div className="comment__bottom">
                                  <div className="heart__icon--coment">
                                    <button onClick={UpdateHandler}>
                                      Update
                                    </button>
                                  </div>
                                </div>
                              </>
                            ) : (
                              <>
                                <p className="comment">{d.comment}</p>
                                <div className="comment__bottom">
                                  <div className="heart__icon--coment">
                                    {token && token.username === d.username && (
                                      <>
                                        <button
                                          value={d.id}
                                          onClick={deleteInputHandler}
                                        >
                                          Delete
                                        </button>
                                        <button
                                          value={d.id}
                                          onClick={inputHandler}
                                        >
                                          Update
                                        </button>
                                      </>
                                    )}
                                  </div>
                                </div>
                              </>
                            )}
                          </div>
                        </form>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        )}
      </div>
    </>
  );
}
export default CommentSection;

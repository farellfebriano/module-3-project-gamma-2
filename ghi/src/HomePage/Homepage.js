import { useGetFavMovieQuery, useGetTokenQuery } from "../store/ApiMovies";
import Banner from "./Banner";
import Nav from "./Nav";
import Row from "./Row";
import "./Homepage.css";
import { useState } from "react";
import RowTopRated from "./RowTopRated";
import Rowpopular from "./RowPopular";
import RowUpcomingMoviesQuery from "./RowUpCommingMovie";
import Footer from "./Footer_Footer";
function Homepage() {
  const { data: content, isLoading } = useGetFavMovieQuery();
  const { data: account } = useGetTokenQuery();
  if (isLoading) {
  } else if (content) {
  }
  // getting the genres fav api id
  const [genresApiId, setGenresApiId] = useState("");
  const genresHandler = (event) => {
    const value = event.target.value;
    setGenresApiId(value);
  };

  return (
    <>
      <div className="homepage">
        <Nav />
        <Banner />
        {account && (
          <nav className="fav-nav">
            <div className="fav-nav_outer">
              <div className="fav-nav__links">
                {content ? (
                  content.favorite_genres.map((genre) => {
                    return (
                      <div key={genre.id}>
                        {/* if the button is clicked it will changed  */}
                        {genresApiId == genre.api_genre_id ? (
                          <button
                            key={genre.id}
                            value={genre.api_genre_id}
                            onClick={genresHandler}
                            className="fav-nav__links__button fav-nav__links__button__hovered"
                          >
                            {genre.name}
                          </button>
                        ) : (
                          <button
                            value={genre.api_genre_id}
                            onClick={genresHandler}
                            className="fav-nav__links__button"
                          >
                            {genre.name}
                          </button>
                        )}
                      </div>
                    );
                  })
                ) : (
                  <></>
                )}
              </div>
            </div>
          </nav>
        )}
        {account && <Row genresApiId={genresApiId} />}
        <h1 className="corousel__title">Top Rated</h1>
        <RowTopRated />
        <h1 className="corousel__title">Popular</h1>
        <Rowpopular />
        <h1 className="corousel__title">Upcoming</h1>
        <RowUpcomingMoviesQuery />
        <Footer />
      </div>
    </>
  );
}
export default Homepage;

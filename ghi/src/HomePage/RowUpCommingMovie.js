import { useEffect, useState } from "react";
import requests from "../request";
import "./Row.css";
import { NavLink } from "react-router-dom";
import {
  useCreateWatchlistMutation,
  useGetWatchListQuery,
  useGetWatchListOnlyIdQuery,
  useGetUpcomingMoviesQuery,
} from "../store/ApiMovies";

function RowUpcomingMoviesQuery({ genresApiId }) {
  const { data: content } = useGetUpcomingMoviesQuery();
  const { data: watchListsOnlyApiId } = useGetWatchListOnlyIdQuery();
  const [createWatchlist, createWatchlistResult] = useCreateWatchlistMutation();

  const movieApiIdHandler = (event) => {
    const movie_api_id = event.target.value;

    createWatchlist({ movie_api_id });
  };

  if (createWatchlistResult.isLoading) {
    console.log(createWatchlistResult);
    console.log("isloading");
  } else if (createWatchlistResult.isSuccess) {
    console.log("succes");
  } else if (createWatchlistResult.error) {
    console.log("Error");
    console.log(createWatchlistResult);
  }
  const { data: movieApiId } = useGetWatchListQuery();

  return (
    <>
      <div className="rowrow">
        <div className="rowrow__posters">
          {content ? (
            content.map((movie) => {
              return (
                <div key={movie.id}>
                  <div className="rowrow__poster__pictures">
                    {watchListsOnlyApiId?.includes(movie.id) ? (
                      <button
                        disabled
                        value={movie.id}
                        onClick={movieApiIdHandler}
                        className=" row__poster__pictures__button row__poster__pictures__button__green"
                      >
                        +
                      </button>
                    ) : (
                      <button
                        value={movie.id}
                        onClick={movieApiIdHandler}
                        className="row__poster__pictures__button "
                      >
                        +
                      </button>
                    )}

                    <NavLink to={`moviedetail/${movie.id}`}>
                      <img
                        key={movie.id}
                        className="rowrow__poster__picture"
                        alt=""
                        src={`${requests.baseImageUrl}${movie.poster_path}`}
                      ></img>
                    </NavLink>
                  </div>
                </div>
              );
            })
          ) : (
            <></>
          )}
        </div>
      </div>
    </>
  );
}
export default RowUpcomingMoviesQuery;

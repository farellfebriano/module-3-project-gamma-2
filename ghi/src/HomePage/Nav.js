import React, { useState, useEffect } from "react";
import { NavLink } from "react-router-dom";

import {
  useLogoutMutation,
  useGetMovieBykeyWordsQuery,
  useGetTokenQuery,
} from "../store/ApiMovies";
import "./Nav.css";
import requests from "../request";

function Nav() {
  const { data: account } = useGetTokenQuery();
  const [logout] = useLogoutMutation();

  const [show, handleShow] = useState(false);

  const transitionNavBar = () => {
    if (window.scrollY > 150) {
      handleShow(true);
    } else {
      handleShow(false);
    }
  };
  const [search, setSearch] = useState("");
  const searcHandler = (event) => {
    const value = event.target.value;
    setSearch(value);
  };
  const resetSearchHandeler = (Event) => {
    setSearch("");
  };

  const { data: movieByKeywords } = useGetMovieBykeyWordsQuery(search);
  console.log(movieByKeywords, "========movieByKeywords=========");
  useEffect(() => {
    window.addEventListener("scroll", transitionNavBar);
    return () => {
      window.removeEventListener("scroll", transitionNavBar);
    };
  }, []);

  return (
    <>
      <div
        className={`header__navigation_h ${
          show && "header__navigation_black_h"
        }`}
      >
        <div className="header__navigation_NavLink-div_h">
          <NavLink to="/" className="header__navigation-NavLink_h">
            MOVIE RAIDERS
          </NavLink>
        </div>

        <nav className="nav_h">
          <div className="nav__links_h">
            <ul>
              <li>
                <input
                  value={search}
                  onChange={searcHandler}
                  className="form-control me-2 "
                  id="searchbar"
                  type="search"
                  placeholder="Search"
                  aria-label="Search"
                ></input>
              </li>
              {!account ? (
                <>
                  <li className="header__navigation-NavLink">
                    <NavLink
                      to="/signup"
                      className="header__navigation-NavLink green"
                      id="positive"
                    >
                      Sign Up
                    </NavLink>
                  </li>
                </>
              ) : (
                <>
                  <li>
                    <NavLink
                      to="/mywatchlist"
                      className="header__navigation-NavLink"
                      onClick={searcHandler}
                      value=""
                    >
                      Watch List
                    </NavLink>
                  </li>
                  <li className="header__navigation-NavLink">
                    <NavLink
                      to="/account"
                      className="header__navigation-NavLink"
                    >
                      Account
                    </NavLink>
                  </li>
                </>
              )}
            </ul>
          </div>
        </nav>
        <div className="authentications_h">
          {account ? (
            <>
              <NavLink
                className="header__navigation-NavLink_h"
                id="negative_h"
                onClick={() => logout()}
              >
                Logout
              </NavLink>
            </>
          ) : (
            <>
              <NavLink
                className="header__navigation-NavLink_h"
                id="positive_h"
                to="/login"
              >
                Login
              </NavLink>
            </>
          )}
        </div>
      </div>
      {search && (
        <div className="search-value-container-center-flex">
          <div className="search-value-container">
            {movieByKeywords?.map((movie) => {
              return (
                <div className="movie__card">
                  {movie.poster_path && (
                    <NavLink
                      to={`/moviedetail/${movie.id}`}
                      onClick={resetSearchHandeler}
                    >
                      <img
                        key={movie.id}
                        className="rowrow__poster__picture__nav"
                        alt=""
                        src={`${requests.baseImageUrl}${movie.poster_path}`}
                      />
                    </NavLink>
                  )}
                </div>
              );
            })}
          </div>
        </div>
      )}
    </>
  );
}

export default Nav;

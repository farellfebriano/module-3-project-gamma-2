import { useState, useEffect } from "react";
import "./Banner.css";
import axios from "../axios";
import requests from "../request";
import { useNavigate } from "react-router-dom";

function Banner() {
  const navigate = useNavigate();
  function truncate(str, n = 150) {
    // in this code means that if str is valid or true and the length of the string
    // is bigger than the given number that return the spliced string
    return str?.length > n ? str.substr(0, n + 1) + "... " : str;
  }

  const [movie, Setmovie] = useState([]);
  useEffect(() => {
    async function fetchData() {
      const request = await axios.get(requests.fetchNowPlaying);
      const content = request;
      return Setmovie(content.data[Math.floor(Math.random() * 5) + 1]);
    }
    fetchData();
  }, []);

  const detailHandeler = (event) => {
    navigate(`/moviedetail/${movie.id}`);
  };
  return (
    <>
      <header
        className="banner"
        style={{
          background: "cover",
          backgroundImage: `url("https://image.tmdb.org/t/p/original${movie?.backdrop_path}")`,
          backgroundPosition: " center center",
          backgroundRepeat: "no-repeat",
        }}
      >
        <div className="banner__contents">
          <h1 className="banner__title">
            {movie?.original_title || movie?.name || movie?.original_name}
          </h1>
          <div className="banner__buttons">
            {/* <button className="banner__button">TRAILER</button> Note dont delete strach goal using modals */}
            <button onClick={detailHandeler} className="banner__button">
              DETAIL
            </button>
          </div>
          <h1 className="banner__description">{truncate(movie?.overview)}</h1>
        </div>
        <div className="banner--fadebottom" />
      </header>
    </>
  );
}
export default Banner;
